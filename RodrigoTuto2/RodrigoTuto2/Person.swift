//
//  Person.swift
//  RodrigoTuto2
//
//  Created by Administrador on 10/02/16.
//  Copyright © 2016 ITESM. All rights reserved.
//

import Foundation

class Person {
    var firstName = ""
    var lastName = ""
    var age = 0
    var email = ""
    
    func input() ->String {
        let keyboard = NSFileHandle.fileHandleWithStandardInput()
        let inputData = keyboard.availableData
        let rawString = NSString(data: inputData, encoding:NSUTF8StringEncoding)
        if let string = rawString {
            return string.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        } else {
            return "Invalid input"
        }
    }
    
    func changeFirstName(newFirstName : String){
        firstName = newFirstName
    }
    
    func enterInfo(){
        print ("What is the first name?")
        firstName = input()
        print ("What is \(firstName)'s last name?")
        lastName = input()
        print ("how old is \(firstName) \(lastName)?")
        let userInput = Int(input())
        if let number = userInput {
            age = number
        }
        print ("What is \(firstName)'s email?")
        email = input()
    }
    
    func printInfo(){
        print ("\(firstName) \(lastName) is \(age) years old and its mail is \(email)")
    }
}



